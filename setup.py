# coding: utf-8

from setuptools import setup


setup(
    name='nvchecker_toolbelt',
    description='Utilities around nvchecker',
    url='https://gitlab.com/yan12125/nvchecker-toolbelt',
    author='Chih-Hsuan Yen',
    author_email='yan12125@gmail.com',
    license='GPLv3',
    packages=['nvchecker_toolbelt'],

    setup_requires=['setuptools_scm'],
    use_scm_version=True,

    install_requires=['nvchecker'],

    classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3 :: Only',
    ],
)
