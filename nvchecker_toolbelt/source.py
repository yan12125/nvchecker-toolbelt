import nvchecker.core


class Source(nvchecker.core.Source):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # KeyManager uses configparser, which ignores non-existent files
        self.keymanager = nvchecker.core.KeyManager('nvchecker.key')

    def on_no_result(self, name):
        self.curvers[name] = None
