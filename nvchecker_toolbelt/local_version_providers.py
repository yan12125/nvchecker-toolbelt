import os.path
import pathlib
import re
import shutil
import tempfile

from .util import get_output


class Handler:
    def __init__(self, root_dirs=None):
        self.root_dirs = root_dirs or []

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, traceback):
        pass


class PKGBUILDHandler(Handler):
    SCRIPT_NAME = 'PKGBUILD'

    @staticmethod
    def comparator(pkg, upstream_ver, pkgbuild_ver):
        parts = pkg.split('-')
        if len(parts) >= 2 and parts[-1] in ('git', 'svn', 'hg'):
            return pkgbuild_ver.startswith(upstream_ver)

        return pkgbuild_ver == upstream_ver

    def get_version(self, pkg):
        for root_dir in self.root_dirs:
            pkgdir = root_dir / pkg
            if pkgdir.exists():
                # Arch Linux SVN repos (packages, community)
                if (pkgdir / 'trunk').exists():
                    pkgdir = pkgdir / 'trunk'
                srcinfo = get_output(['makepkg', '--printsrcinfo'], cwd=pkgdir)
                return re.search(r'pkgver\s*=\s*(.+)', srcinfo).group(1)

        raise ValueError(f'Package {pkg} not found in {self.root_dirs}')


class PortfileHandler(Handler):
    SCRIPT_NAME = 'Portfile'

    def __init__(self, default_sources=None, **kwargs):
        super(PortfileHandler, self).__init__(**kwargs)

        self.tmp_dir = None

    def __enter__(self):
        self.tmp_dir = pathlib.Path(tempfile.mkdtemp())

        with open(self.tmp_dir / 'macports.conf', 'w') as f:
            f.write(f'sources_conf {self.tmp_dir / "sources.conf"}')

        shutil.copy2(
            '/opt/local/etc/macports/sources.conf',
            self.tmp_dir / 'sources.conf')

        with open(self.tmp_dir / 'sources.conf', 'a') as f:
            for root_dir in self.root_dirs:
                f.write(f'file:///{root_dir}')

        return self

    def __exit__(self, exc_type, exc_value, traceback):
        shutil.rmtree(self.tmp_dir)

    def get_version(self, pkg):
        if self.tmp_dir is None:
            raise RuntimeError('PortfileHandler should be used as a context manager')

        env = os.environ.copy()
        env['PORTSRC'] = self.tmp_dir / 'macports.conf'

        return get_output([
            '/opt/local/bin/port', '-q', 'info', '--version', pkg
        ], env=env)
