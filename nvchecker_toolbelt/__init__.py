import asyncio
import json
import logging

from .source import Source

logger = logging.getLogger(__name__)


def check_pkg(pkg, handler, upstream_ver, tolerate):
    logger.info('Checking ' + pkg)

    pkgver = handler.get_version(pkg)

    try:
        comparator = handler.comparator
    except AttributeError:
        def comparator(pkg, upstream_ver, pkgver):
            return upstream_ver == pkgver

    if comparator(pkg, upstream_ver, pkgver):
        return True

    logger.info('\n'.join((
        'Version mismatch:',
        f'\t{handler.SCRIPT_NAME} version = {pkgver}',
        f'\tUpstream version = {upstream_ver}',
    )))

    if pkg in tolerate:
        if (tolerate[pkg]['local_version'] == pkgver and
                tolerate[pkg]['remote_version'] == upstream_ver):
            logger.info('Tolerated: ' + tolerate[pkg]['note'])
            return True

    return False


def check_pkgs(handler, conf_file: str = 'nvchecker.ini'):
    try:
        with open('check_all_tolerate.json', 'r') as f:
            tolerate = json.load(f)
    except FileNotFoundError:
        tolerate = {}

    all_up_to_date = True

    logger.info('Running nvchecker...')

    with open(conf_file, 'r') as f:
        s = Source(f)
        asyncio.get_event_loop().run_until_complete(s.check())
    with handler:
        for pkg in sorted(s.curvers.keys()):
            this_up_to_date = check_pkg(pkg, handler, s.curvers[pkg], tolerate)
            all_up_to_date = all_up_to_date and this_up_to_date

    if not all_up_to_date:
        return 1
    return 0
