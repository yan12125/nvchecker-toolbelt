import subprocess


def get_output(cmd, **kwargs):
    return subprocess.check_output(cmd, **kwargs).decode('utf-8').strip()
